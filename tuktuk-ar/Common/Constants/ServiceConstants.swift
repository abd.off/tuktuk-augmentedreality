//
//  ServiceConstants.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation

enum ServiceConstants {
  static let baseUrl = URL(string: "https://api.geoapify.com/v2/places")
  static let apiKey = "55b09456572e4841aff810c6124c7625"
  static let requestTimeout: TimeInterval = 10
  static let resourceTimeout: TimeInterval = 10
  static let validCodes = (200 ..< 300)
  static let limitOffset = 50
}
