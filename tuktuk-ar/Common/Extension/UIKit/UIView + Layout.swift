//
//  UIView + Layout.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import UIKit

struct Layout {
  
  let element: UIView
  
  init(_ element: UIView) {
    self.element = element
  }
  
  // MARK: Layout
  
  @discardableResult func size(_ value: CGFloat) -> [NSLayoutConstraint] {
    let widthConstraint = width(value)
    let heightConstraint = height(value)
    let constraints = [widthConstraint, heightConstraint]
    safeSuperview().addConstraints(constraints)
    return constraints
  }
  
  @discardableResult func width(_ value: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0, constant: value)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func height(_ value: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                         attribute: .height,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0, constant: value)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinHorizontalEdgesToSuperView(padding: CGFloat = 0) -> [NSLayoutConstraint] {
    let constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(padding)-[view]-(padding)-|",
                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                     metrics: ["padding": padding],
                                                     views: ["view": element])
    safeSuperview().addConstraints(constraints)
    return constraints
  }
  
  @discardableResult func pinVerticalEdgesToSuperView(padding: CGFloat = 0) -> [NSLayoutConstraint] {
    let constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(padding)-[view]-(padding)-|",
                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                     metrics: ["padding": padding],
                                                     views: ["view": element])
    safeSuperview().addConstraints(constraints)
    return constraints
  }
  
  @discardableResult func centerVertically() -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .centerY,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .centerY,
                                        multiplier: 1.0, constant: 0)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func centerHorizontally() -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .centerX,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .centerX,
                                        multiplier: 1.0, constant: 0)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinLeadingToSuperview(constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .leading,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .leading,
                                        multiplier: 1, constant: constant)
    
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinTrailingToSuperview(constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .trailing,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .trailing,
                                        multiplier: 1, constant: constant)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinTopToSuperview(constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .top,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .top,
                                        multiplier: 1, constant: constant)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinBottomToSuperview(constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: safeSuperview(),
                                        attribute: .bottom,
                                        multiplier: 1, constant: constant)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinTopToView(view:UIView,constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .top,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .bottom,
                                        multiplier: 1, constant: constant)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  @discardableResult func pinBottomToView(view:UIView,constant: CGFloat) -> NSLayoutConstraint {
    let constraint = NSLayoutConstraint(item: element,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .top,
                                        multiplier: 1, constant: constant)
    safeSuperview().addConstraint(constraint)
    return constraint
  }
  
  func fillSuperview(padding: CGFloat = 0) {
    safeSuperview()
    pinHorizontalEdgesToSuperView(padding: padding)
    pinVerticalEdgesToSuperView(padding: padding)
  }
  
  @discardableResult private func safeSuperview() -> UIView {
    element.translatesAutoresizingMaskIntoConstraints = false
    guard let view = element.superview else {
      fatalError("You need to have a superview before you can add contraints")
    }
    return view
  }
}

extension UIView {
  var layout: Layout {
    return Layout(self)
  }
}
