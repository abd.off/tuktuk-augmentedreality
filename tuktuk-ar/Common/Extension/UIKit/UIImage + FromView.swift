//
//  UIImage + FromView.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import UIKit

extension UIImage {
  class func image(from view: UIView) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    
    view.layer.render(in: context)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return img
  }
}
