//
//  CGPoint.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import SceneKit

extension CGPoint {
  var positionIn3D: SCNVector3 {
    return SCNVector3(y, 0.0, x)
  }
  
  init(position: SCNVector3) {
    self.init(x: CGFloat(position.z), y: CGFloat(position.x))
  }
}
