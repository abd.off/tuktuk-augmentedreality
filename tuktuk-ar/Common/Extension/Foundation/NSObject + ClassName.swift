//
//  NSObject + ClassName.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation

extension NSObject {
  static var className: String {
    return String(describing: self)
  }
  var className: String {
    return String(describing: type(of: self))
  }
}
