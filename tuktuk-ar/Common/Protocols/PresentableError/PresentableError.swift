//
//  PresentableError.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation

protocol PresentableError where Self: Error {
  var userMessage: String { get }
  var isNetworkError: Bool { get }
}

extension APIError {
  var isNetworkError: Bool {
    switch self {
    case .noNetwork:    return true
    default:            return false
    }
  }
}
