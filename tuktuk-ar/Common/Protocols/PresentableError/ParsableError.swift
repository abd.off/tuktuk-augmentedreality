//
//  ParsableError.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation

struct ParsableError: Decodable, PresentableError {
  
  // MARK: - Properties
  
  private let message: String
  let type: String
  let errors: [DetailError]?
  
  // MARK: - Nested struct
  
  struct DetailError: Decodable {
    let name: String
    let message: String
  }
  
  // MARK: - PresentableError
  
  var userMessage: String {
    return message
  }
  
  var isNetworkError: Bool {
    return false
  }
}
