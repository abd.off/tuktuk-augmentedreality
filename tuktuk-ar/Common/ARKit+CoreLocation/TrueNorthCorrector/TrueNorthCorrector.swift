//
//  TrueNorthCorrector.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation

func correctionAngleByBearing(_ e1: SceneLocationEstimate, _ e2: SceneLocationEstimate) -> Double {
  let calculatedE2Location = e1.translatedLocation(to: e2.position)
  return bearingBetween(e1.location.coordinate, calculatedE2Location.coordinate) - bearingBetween(e1.location.coordinate, e2.location.coordinate)
}
