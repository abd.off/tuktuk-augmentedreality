//
//  GeoExtensions.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation
import CoreLocation
import SceneKit
import MapKit

struct GeometryConstants {
  static let EarthRadius = Double(6_371_000)
  static let LatLonEps = 1e-6
}

extension Double {
  
  func metersToLatitude() -> Double {
    return self / GeometryConstants.EarthRadius
  }
  
  func metersToLongitude(lat: Double) -> Double {
    return self / GeometryConstants.EarthRadius * cos(lat.degreesToRadians)
  }
}

extension BinaryFloatingPoint {
  var degreesToRadians: Self { return self * .pi / 180 }
  var radiansToDegrees: Self { return self * 180 / .pi }
}

struct SceneLocationEstimate {
  let location: CLLocation
  let position: SCNVector3
  init(location: CLLocation, position: SCNVector3) {
    self.location = location
    self.position = position
  }
}

extension SceneLocationEstimate {
  
  func translatedLocation(to position: SCNVector3) -> CLLocation {
    let translation = position - self.position
    let translatedCoordinate = location.coordinate.transform(using: CLLocationDistance(-translation.z),
                                                             longitudinalMeters: CLLocationDistance(translation.x))
    return CLLocation(
      coordinate: translatedCoordinate,
      altitude: location.altitude,
      horizontalAccuracy: location.horizontalAccuracy,
      verticalAccuracy: location.verticalAccuracy,
      timestamp: location.timestamp
    )
  }
}


/// Haversine formula to calculate the great-circle distance between two points
 func metersBetween(_ lat1: Double, _ lon1: Double, _ lat2: Double, _ lon2: Double) -> Double {
  let sqr: (Double) -> Double = { $0 * $0 }
  let R = GeometryConstants.EarthRadius // meters
  
  let phi_1 = lat1.degreesToRadians
  let phi_2 = lat2.degreesToRadians
  let dPhi = (lat2 - lat1).degreesToRadians
  let dLmb = (lon2 - lon1).degreesToRadians
  
  let a = sqr(sin(dPhi/2)) + cos(phi_1) * cos(phi_2) * sqr(sin(dLmb/2))
  let c: Double = 2 * atan2(sqrt(a), sqrt(Double(1) - a))
  
  return R * c
}

 func metersBetween(_ coordinate1: CLLocationCoordinate2D, _ coordinate2: CLLocationCoordinate2D) -> Double {
  return metersBetween(coordinate1.latitude, coordinate1.longitude, coordinate2.latitude, coordinate2.longitude)
}

//(-180,180] anticlockwise is positive
 func bearingBetween(_ point1: CLLocationCoordinate2D, _ point2: CLLocationCoordinate2D) -> Double {
  let lat1 = point1.latitude.degreesToRadians
  let lon1 = point1.longitude.degreesToRadians
  
  let lat2 = point2.latitude.degreesToRadians
  let lon2 = point2.longitude.degreesToRadians
  
  let dLon = lon2 - lon1
  
  let y = sin(dLon) * cos(lat2)
  let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
  let radiansBearing = atan2(y, x)
  
  return radiansBearing.radiansToDegrees
}

/// Translation in meters between 2 locations
 struct LocationTranslation {
  var latitudeTranslation: Double
  var longitudeTranslation: Double
  
  init(latitudeTranslation: Double, longitudeTranslation: Double) {
    self.latitudeTranslation = latitudeTranslation
    self.longitudeTranslation = longitudeTranslation
  }
}

extension LocationTranslation {
  init(dLat: Double, dLon: Double) {
    self.init(latitudeTranslation: dLat, longitudeTranslation: dLon)
  }
  
  var dLat: Double {
    return latitudeTranslation
  }
  
  var dLon: Double {
    return longitudeTranslation
  }
}

extension CLLocationCoordinate2D {
  
  var lat: Double {
    return latitude
  }
  
  var lon: Double {
    return longitude
  }
  
  func transform(using latitudinalMeters: CLLocationDistance, longitudinalMeters: CLLocationDistance) -> CLLocationCoordinate2D {
    let region = MKCoordinateRegion(center: self, latitudinalMeters: latitudinalMeters, longitudinalMeters: longitudinalMeters)
    return CLLocationCoordinate2D(latitude: latitude + region.span.latitudeDelta, longitude: longitude + region.span.longitudeDelta)
  }
  
  /// Calculate translation between to coordinates
  func translation(toCoordinate coordinate: CLLocationCoordinate2D) -> LocationTranslation {
    let position = CLLocationCoordinate2D(latitude: self.latitude, longitude: coordinate.longitude)
    let distanceLat = metersBetween(coordinate, position)
    let dLat: Double = (coordinate.lat > position.lat ? 1 : -1) * distanceLat
    let distanceLon = metersBetween(self, position)
    let dLon: Double = (lon > position.lon ? -1 : 1) * distanceLon
    return LocationTranslation(dLat: dLat, dLon: dLon)
  }
}
