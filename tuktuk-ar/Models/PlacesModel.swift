//
//  PlacesModel.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation

// MARK: - PlacesModel

struct PlacesModel: Decodable {
  let type: String
  let features: [Feature]
}

// MARK: - Feature

struct Feature: Decodable {
  let properties: Properties
  let geometry: Geometry
}

// MARK: - Geometry

struct Geometry: Decodable {
  let coordinates: [Double]
}

// MARK: - Properties

struct Properties: Decodable {
  let name: String?
  let street: String
  let city: String
  let county: String
  let state: String
  let region: String
  let postcode: String
  let country: String
  let countryCode: String
  let lon, lat: Double
  let formatted, addressLine2: String
  let distance: Int
  let placeID: String
  let housenumber, neighbourhood: String?
  
  enum CodingKeys: String, CodingKey {
    case name, street, city, county, state, region, postcode, country
    case countryCode = "country_code"
    case lon, lat, formatted
    case addressLine2 = "address_line2"
    case distance
    case placeID = "place_id"
    case housenumber, neighbourhood
  }
}
