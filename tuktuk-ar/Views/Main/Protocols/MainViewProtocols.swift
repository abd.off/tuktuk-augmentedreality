//
//  MainViewProtocols.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation

protocol MainViewProtocol: AlertPresentable, Loadable {
  func update(with model: PlacesModel)
}

protocol MainViewPresenterProtocol {
  func getPlaces()
  func didSelectPlace(_ feature: Feature)
}

protocol MainViewDelegate: AnyObject {
  func didSelectPlace(_ feature: Feature)
}
