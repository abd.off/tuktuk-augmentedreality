//
//  MainViewController.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit

final class MainViewController: UIViewController, MainViewProtocol, MainViewDelegate {
  
  // MARK: - Properties
  
  private var model: PlacesModel?
  var presenter: MainViewPresenterProtocol?
  
  // MARK: - Views
  
  private lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .white
    tableView.separatorStyle = .none
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(cellTypes: [PlaceCell.self])
    return tableView
  }()
  
  // MARK: - Life Circle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubViews()
    presenter?.getPlaces()
  }
  
  // MARK: - Set UI
  
  private func addSubViews() {
    view.addSubview(tableView)
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
    navigationController?.navigationBar.prefersLargeTitles = true
  }
  
  // MARK: - Set Layout
  
  override func updateViewConstraints() {
    tableView.layout.fillSuperview()
    super.updateViewConstraints()
  }
    
  // MARK: - MainViewProtocol
  
  func update(with model: PlacesModel) {
    DispatchQueue.main.async {
      self.model = model
      self.tableView.animationReload()
    }
  }
  
  // MARK: - MainViewDelegate
  
  func didSelectPlace(_ feature: Feature) {
    presenter?.didSelectPlace(feature)
  }
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return model?.features.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: PlaceCell.identifier, for: indexPath) as? PlaceCell,
          let feature = model?.features[indexPath.row] else { return UITableViewCell() }
    cell.feature = feature
    cell.viewDelegate = self
    return cell
  }
}
