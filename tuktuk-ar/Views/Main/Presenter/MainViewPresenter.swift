//
//  MainViewPresenter.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation
import CoreLocation

final class MainViewPresenter: MainViewPresenterProtocol {
  
  // MARK: - Properties
  
  private var networkService: MainService
  weak var view: MainViewProtocol?
  let router: RouterProtocol?
  
  // MARK: - Init
  
  init(view: MainViewProtocol, router: RouterProtocol, networkService: MainService) {
    self.view = view
    self.router = router
    self.networkService = networkService
  }
  
  // MARK: - MainPresenterProtocol
  
  func getPlaces() {
    guard let location = NativeLocationManager.sharedInstance.location  else {
      view?.showAlert(title: "Error", message: "location is not found =(", actions: [AlertCancelAction], style: .alert)
      return
    }
    
    view?.showLoading()
    networkService.getPlaces(PlaceServiceModel(userCoordinate: location.coordinate)) { [weak self] result in
      guard let self = self else { return }
      switch result {
      case .success(let value):
        self.view?.hideLoading()
        self.view?.update(with: value)
      case .failure(let error):
        self.view?.hideLoading()
        self.view?.showAlert(title: "Error", message: error.userMessage, actions: [AlertOkAction], style: .alert)
      }
    }
  }
  
  func didSelectPlace(_ feature: Feature) {
    let latitude = feature.geometry.coordinates.last ?? 0
    let longitude = feature.geometry.coordinates.first ?? 0
    let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    router?.openRouteScreen(coordinate)
  }
}
